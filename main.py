import chess
import chess.uci

engine = chess.uci.popen_engine("/Users/pierre/PycharmProjects/Stockfish/src/stockfish")
engine.uci()
engine.author  # doctest: +SKIP

board = chess.Board("1k1r4/pp1b1R2/3q2pp/4p3/2B5/4Q3/PPP2B2/2K5 b - - 0 1")
print(board)
print(board.fen())
engine.position(board)
engine.go(movetime=2000) # Gets tuple of bestmove and ponder move.

def callback(command):
    bestmove, ponder = command.result()
    assert bestmove == chess.Move.from_uci('d6d1')
    board.push(bestmove)

command = engine.go(movetime=2000, async_callback=callback)
command.done()

a = command.result()
print a
command.done()
print(board)
print(board.fen())

engine.quit()