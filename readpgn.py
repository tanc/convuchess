import chess
import chess.pgn
from Pgn import Pgn

SEP = '__________________'
def getGame(node):
    while not node.is_end():
        next_node = node.variation(0)
        """ Liste des coups au format classique PGN """
        #print(node.board().san(next_node.move))
        """ Liste des coups au format FEN """
        print(node.board().fen())
        """ Transformation d'un board en matrice"""
        print(SEP)
        print(node.board())
        node = next_node

def getPgn(Path,number):
    pgn = open(Path)
    pgn.seek(number)
    vgame = chess.pgn.read_game(pgn)
    my_pgn = Pgn(Path, vgame.headers["White"], vgame.headers["WhiteElo"], vgame.headers["Black"], vgame.headers["BlackElo"], vgame.headers["Result"])
    print my_pgn
    getGame(my_pgn)
    pgn.close()
    return my_pgn,vgame

def getAllPgn(Path):
    pgn = open(Path)
    tentacle_white_win_offsets = (offset for offset, headers in chess.pgn.scan_headers(pgn) if headers["Result"] == "1-0")
    for i in range(tentacle_white_win_offsets.__sizeof__()):
        next_tentacle_white_win = next(tentacle_white_win_offsets)
        print next_tentacle_white_win
        getPgn(Path,next_tentacle_white_win)

def getAllDirectPgn(Path):
    pgn = open(Path)
    tentacle_white_win_offsets = (offset for offset, headers in chess.pgn.scan_headers(pgn))
    for i in range(tentacle_white_win_offsets.__sizeof__()):
        vgame = chess.pgn.read_game(pgn)
        my_pgn = Pgn(Path, vgame.headers["White"], vgame.headers["WhiteElo"], vgame.headers["Black"],
                     vgame.headers["BlackElo"], vgame.headers["Result"])
        print my_pgn
        getGame(vgame)


def fen_to_matrix(fens):
    arrays = []
    for fen in fens:
        a = imread(fname, flatten=1)
        arrays.append(a)
    data = np.array(arrays)

def main():
    Path = "/Users/pierre/PycharmProjects/convochess/data/pgn/ficsgamesdb_tentacle_2016_nomovetimes.pgn"
    getAllDirectPgn(Path)



main()
