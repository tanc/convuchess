class WhitePlayer:
    """Classe definissant un joueur :
Pour simplifier dans un premier temps, on dira qu'un joueur a
- un nom
- un elo
- une couleur"""

    def __init__(self, whitename, whiteelo, whitecolor):
        self.whitename = whitename
        self.whiteelo = whiteelo
        self.whitecolor = whitecolor

    def showwhite(self):
        return "%s : %s / %s " % \
              (self.whitecolor, self.whitename, self.whiteelo)

class BlackPlayer:
    """Classe definissant un joueur :
Pour simplifier dans un premier temps, on dira qu'un joueur a
- un nom
- un elo
- une couleur"""

    def __init__(self, blackname, blackelo, blackcolor):
        self.blackname = blackname
        self.blackelo = blackelo
        self.blackcolor = blackcolor

    def showblack(self):
        return "%s : %s / %s " % \
              (self.blackcolor, self.blackname, self.blackelo)

class Pgn(WhitePlayer,BlackPlayer):
    """Classe definissant un pgn :
[Event "FICS rated standard game"]
[Site "FICS freechess.org"]
[FICSGamesDBGameNo "371408112"]
[White "birdcostello"]
[Black "Pfizer"]
[WhiteElo "2721"]
[BlackElo "2426"]
[WhiteIsComp "Yes"]
[TimeControl "900+0"]
[Date "2015.02.01"]
[Time "02:10:00"]
[WhiteClock "0:15:00.000"]
[BlackClock "0:15:00.000"]
[ECO "D37"]
[PlyCount "181"]
[Result "1-0"]"""

    def __init__(self, path, whitename, whiteelo, blackname, blackelo, gameresult):
        """Constructeur de notre classe"""
        self.path = path
        self.gameresult = gameresult
        WhitePlayer.__init__(self, whitename, whiteelo, "white")
        BlackPlayer.__init__(self, blackname, blackelo, "black")


    def __repr__(self):
        return "%s vs %s : %s" % \
              ( self.showwhite(),
                self.showblack(),
                self.gameresult)

    def getPath(self):
        return self.path